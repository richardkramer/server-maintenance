# Server Maintenance

Various tools to maintain and monitor private servers.

## Portainer

### Agent Key

Portainer cannot decode the agent key with a different edge url than the frontend url. Decode and reencode the key with
the correct urls [here](https://www.base64encode.org/).

## Prometheus

### Volume Permissions

Create volume dir and set correct permissions **before** starting the container:

```sh
sudo mkdir -p .volumes/prometheus/data
sudo chown -R 65534:65534 .volumes/prometheus/data
```

### Memory CGroup

Modify this setting in `/etc/default/grub`, then run `sudo update-grub`.

```grub
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1 systemd.unified_cgroup_hierarchy=false"
```

### BasicAuth Credentials

Generate a bcrypt encrypted password with this command:

```sh
htpasswd -nBC 10 "" | cut -d':' -f2
```

You also have to create the file `config/prometheus/basic_auth_password` with the plain text password inside of it, so
prometheus can scrape itself.

<!-- ### TLS Certificate

Create client authentication certificates with this command:

```sh
sudo rm -rf ./config/prometheus/certs
sudo mkdir -p ./config/prometheus/certs
sudo openssl req -new -newkey rsa:4096 -sha256 -nodes -subj "/CN=$(hostname)" -addext "subjectAltName=DNS:prometheus" -out ./config/prometheus/certs/server.csr -keyout ./config/prometheus/certs/server.key
sudo openssl req -new -newkey rsa:4096 -x509 -sha256 -days 36500 -nodes -subj "/CN=ca.$(hostname)" -addext "subjectAltName=DNS:ca.prometheus" -out ./config/prometheus/certs/ca.crt -keyout ./config/prometheus/certs/ca.key
sudo bash -c 'openssl x509 -req -sha256 -days 36500 -in ./config/prometheus/certs/server.csr -CAkey ./config/prometheus/certs/ca.key -CA ./config/prometheus/certs/ca.crt -set_serial -01 -out ./config/prometheus/certs/server.crt -extfile <(printf "subjectAltName=DNS:prometheus")'
sudo chown -R 65534:65534 config/prometheus/certs
sudo bash -c 'chmod 400 config/prometheus/certs/*'

gum style --underline --foreground 2 --padding '1 0' --width 80 --align center --border rounded --border-foreground 2 -- "CA cert"
sudo cat config/prometheus/certs/ca.crt
gum style --underline --foreground 2 --padding '1 0' --width 80 --align center --border rounded --border-foreground 2 -- "Server cert"
sudo cat config/prometheus/certs/server.crt
gum style --underline --foreground 2 --padding '1 0' --width 80 --align center --border rounded --border-foreground 2 -- "Server Key"
sudo cat config/prometheus/certs/server.key
```

Then use the certificate together with the output of `hostname` (or "prometheus") as ServerName for authentication. -->
